// package main

// import (
// 	"bufio"
// 	"fmt"
// 	"os"
// )

// func main() {
// 	scanner := bufio.NewScanner(os.Stdin)
// 	for scanner.Scan() {
// 		fmt.Println(scanner.Text()) // Println will add back the final '\n'
// 	}
// 	if err := scanner.Err(); err != nil {
// 		fmt.Fprintln(os.Stderr, "reading standard input:", err)
// 	}
// }

package main

import (
	"fmt"
	"io"
	"net"

	log "github.com/sirupsen/logrus"
)

func main() {

	l, err := net.Listen("tcp4", ":4567")

	if err != nil {
		log.Errorf("Can't start TCP server : %v\n", err)
	}
	defer l.Close()

	log.Infoln("Starting TCP server...")

	for {
		conn, err := l.Accept()
		if err != nil {
			log.Errorf("Can't accept incoming conn: %v\n", err)
		}
		buf := make([]byte, 5)
		for {
			n, err := conn.Read(buf)
			log.Infof("Bytes number %v", n)
			log.Infof("Capacity %v", cap(buf))

			if err != nil {
				if err != io.EOF {
					fmt.Println("read error:", err)
				}
				break
			}
			message := string(buf[:n])
			log.Infof("%v", message)
			if n >= cap(buf) {
				size := int32(cap(buf) * 2)
				buf = make([]byte, size)
			} else if n <= cap(buf) && n < cap(buf)/2 {
				size := int32(cap(buf) / 2)
				buf = make([]byte, size)
			}
		}
	}
}
