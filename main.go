package main

import (
	"bufio"
	"fmt"
	"net"
	"strings"
	"sync/atomic"

	log "github.com/sirupsen/logrus"
)

const (
	CMD_START = iota
	CMD_PAYLOAD
)

type (
	client struct {
		id            uint32
		pipe          chan []string
		conn          net.Conn
		subs          []string
		state         int
		nextBroadcast []string
	}

	// sub struct {
	// 	label string
	// }
)

var clients = []*client{}
var uid uint32

func newClient(conn net.Conn) *client {
	c := &client{
		id:            atomic.AddUint32(&uid, 1),
		pipe:          make(chan []string),
		conn:          conn,
		subs:          []string{},
		nextBroadcast: []string{},
	}
	return c
}

/*
 * Voir d'accrocher un client sur les subscriptions plutot, et de publier sur ces subscriptions qui
 * distribuerons à chacun de leur clients
 * essayer de garder les messages en memoire le temps que les clients arrivent
 *
 */
func (c *client) parse(cmd string) error {

	switch c.state {
	case CMD_START:
		fields := strings.Fields(cmd)
		fieldsLen := len(fields)
		if fieldsLen < 1 {
			return fmt.Errorf("Parse Error")
		}
		switch fields[0] {
		case "BROADCAST":
			if fieldsLen != 3 {
				return fmt.Errorf("Parse Error")
			}
			c.nextBroadcast = fields
			c.state = CMD_PAYLOAD
		case "LISTEN":
			if fieldsLen != 2 {
				return fmt.Errorf("Parse Error")
			}
			c.listen(fields[1])
			c.state = CMD_START
		default:
			return fmt.Errorf("Parse Error")
		}
	case CMD_PAYLOAD:
		//verifier la taille du payload
		c.broadcast(c.nextBroadcast, cmd)
		c.state = CMD_START
	}
	return nil
	//log.Infof("Received %v \n", strings.Fields(cmd))
	//log.Infof("Received %v len : %v\n", cmd, len(cmd))

}

func (c *client) broadcast(cmd []string, payload string) {
	for _, client := range clients {
		if client.id != c.id {
			msg := []string{cmd[1], payload}
			client.pipe <- msg
		}
	}
}

func (c *client) listen(subject string) {
	c.subs = append(c.subs, subject)
}

func (c *client) read() {
	s := bufio.NewScanner(c.conn)

	for {
		ok := s.Scan()
		if !ok {
			break
		}

		err := c.parse(s.Text())
		if err != nil {
			log.Errorf("%s\n", err.Error())
		}
	}
	log.Infof("Client %d disconnected\n", c.id)
	//delete from client list
}

func (c *client) writeMsg() {
	for msg := range c.pipe {
		for _, sub := range c.subs {
			if sub == msg[0] {
				r := fmt.Sprintf("MESSAGE %s\n%s\n", msg[0], msg[1])
				c.conn.Write([]byte(r))
			}
		}
	}
}

// func (s *sub) match(label string) bool {
// 	if s.label == label {
// 		return true
// 	}
// 	return false
// }

func main() {

	l, err := net.Listen("tcp4", ":4567")

	if err != nil {
		log.Errorf("Can't start TCP server : %v\n", err)
	}
	defer l.Close()

	log.Infoln("Starting TCP server...")

	for {
		conn, err := l.Accept()
		if err != nil {
			log.Errorf("Can't accept incoming conn: %v\n", err)
		}

		c := newClient(conn)
		log.Infof("New client connected with given id :%d\n", c.id)
		clients = append(clients, c)
		go c.read()
		go c.writeMsg()
	}
}
